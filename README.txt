
DESCRIPTION
-----------

Integrate Bounce and Simplenews modules.


REQUIREMENTS
------------

 * Bounce and Simplenews modules.


INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "bounce_simplenews" in the sites/all/modules
    directory and place the entire contents of this folder in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page.


DOCUMENTATION
-------------

None at this present time.
